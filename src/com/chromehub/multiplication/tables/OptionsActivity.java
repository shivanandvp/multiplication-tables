package com.chromehub.multiplication.tables;

import android.os.Bundle;
import android.preference.PreferenceManager;
import android.app.Activity;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.support.v4.app.NavUtils;

public class OptionsActivity extends Activity {
	
	private static SharedPreferences sharedPreferences;
	private static SharedPreferences.Editor editor;
	private static int num1Min;
	private static int num1Max;
	private static int num2Min;
	private static int num2Max;	
	private static EditText editTextNum1Min, editTextNum1Max, editTextNum2Min, editTextNum2Max;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_options);     
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        editor = sharedPreferences.edit();
        num1Min = sharedPreferences.getInt("num1Min", ReferActivity.NUM1MIN);
		num1Max = sharedPreferences.getInt("num1Max", ReferActivity.NUM1MAX);
		num2Min = sharedPreferences.getInt("num2Min", ReferActivity.NUM2MIN);
		num2Max = sharedPreferences.getInt("num2Max", ReferActivity.NUM2MAX);	
		editTextNum1Min = (EditText) findViewById(R.id.editText1);
		editTextNum1Max = (EditText) findViewById(R.id.EditText01);
		editTextNum2Min = (EditText) findViewById(R.id.EditText02);
		editTextNum2Max = (EditText) findViewById(R.id.EditText03);
		editTextNum1Min.setText(String.valueOf(num1Min));
		editTextNum1Max.setText(String.valueOf(num1Max));
		editTextNum2Min.setText(String.valueOf(num2Min));
		editTextNum2Max.setText(String.valueOf(num2Max));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_options, menu);
        return true;
    }

    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
    
    public void resetButtonClicked(View view){
    	editTextNum1Min.setText(String.valueOf(ReferActivity.NUM1MIN));
    	editTextNum1Max.setText(String.valueOf(ReferActivity.NUM1MAX));		
    	editTextNum2Min.setText(String.valueOf(ReferActivity.NUM2MIN));
    	editTextNum2Max.setText(String.valueOf(ReferActivity.NUM2MAX));
    	editor.putInt("num1Min",ReferActivity.NUM1MIN);
    	editor.putInt("num1Max",ReferActivity.NUM1MAX);		
    	editor.putInt("num2Min",ReferActivity.NUM2MIN);
    	editor.putInt("num2Max",ReferActivity.NUM2MAX);
    	editor.commit();  
    }
    
    public void applyButtonClicked(View view){    	 
    	editor.putInt("num1Min",Integer.parseInt(editTextNum1Min.getText().toString()));
    	editor.putInt("num1Max",Integer.parseInt(editTextNum1Max.getText().toString()));		
    	editor.putInt("num2Min",Integer.parseInt(editTextNum2Min.getText().toString()));
    	editor.putInt("num2Max",Integer.parseInt(editTextNum2Max.getText().toString()));
    	editor.commit();    	
    }
}
