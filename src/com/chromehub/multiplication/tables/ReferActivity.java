package com.chromehub.multiplication.tables;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.NavUtils;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;
import android.widget.ViewFlipper;
import android.view.View.OnTouchListener;

public class ReferActivity extends Activity implements OnGestureListener {

	private static SharedPreferences sharedPreferences;
	private static GestureDetector gestureScanner;
	private static ViewFlipper viewFlipper;	
	private static ListView listView;
	private static int num1Min;
	private static int num1Max;
	private static int num2Min;
	private static int num2Max;	
	public static final int NUM1MIN = 1;
	public static final int NUM1MAX = 20;
	public static final int NUM2MIN = 1;
	public static final int NUM2MAX = 10;
	public static final int FLING_THRESHOLD = 120;
	private ArrayAdapter<String> arrayAdapter;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);		
		setContentView(R.layout.activity_refer);		
		viewFlipper = (ViewFlipper) findViewById(R.id.viewFlipper1);		
		gestureScanner = new GestureDetector(this, this);
		sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
		num1Min = sharedPreferences.getInt("num1Min", NUM1MIN);
		num1Max = sharedPreferences.getInt("num1Max", NUM1MAX);
		num2Min = sharedPreferences.getInt("num2Min", NUM2MIN);
		num2Max = sharedPreferences.getInt("num2Max", NUM2MAX);						
		for (int i = num1Min; i <= num1Max; i++) {			
			String [] line = new String[num2Max-num2Min + 1];
			for(int j=num2Min; j<=num2Max; j++){
				line[j-num2Min] = String.valueOf(i) + " x " + String.valueOf(j) + " = " + String.valueOf(i*j);
			}        
			arrayAdapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,line);
			listView = new ListView(this);
	        listView.setAdapter(arrayAdapter);
	        listView.setOnTouchListener(new OnTouchListener() {
	            public boolean onTouch(View view, MotionEvent e) {
	                gestureScanner.onTouchEvent(e);
	                return false;
	            }
	        });
	        viewFlipper.addView(listView, new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
		}	
		Toast.makeText(getApplicationContext(),"Swipe left or right", Toast.LENGTH_SHORT).show();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_refer, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	public boolean onDown(MotionEvent arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	public boolean onFling(MotionEvent arg0, MotionEvent arg1, float arg2,
			float arg3) {
		if (arg0.getX() - arg1.getX() > FLING_THRESHOLD) {

			//this.viewFlipper.setInAnimation(AnimationUtils.loadAnimation(this,R.anim.push_left_in));
			//this.viewFlipper.setOutAnimation(AnimationUtils.loadAnimation(this,R.anim.push_left_out));
			viewFlipper.showNext();
			return true;
		} else if (arg0.getX() - arg1.getX() < -FLING_THRESHOLD) {
			//this.viewFlipper.setInAnimation(AnimationUtils.loadAnimation(this,R.anim.push_right_in));
			//this.viewFlipper.setOutAnimation(AnimationUtils.loadAnimation(this,R.anim.push_right_out));
			viewFlipper.showPrevious();
			return true;
		}
		return true;
	}

	public void onLongPress(MotionEvent arg0) {
		// TODO Auto-generated method stub
	}

	public boolean onScroll(MotionEvent arg0, MotionEvent arg1, float arg2,
			float arg3) {
		// TODO Auto-generated method stub
		return true;
	}

	public void onShowPress(MotionEvent arg0) {
		// TODO Auto-generated method stub
	}

	public boolean onSingleTapUp(MotionEvent arg0) {
		// TODO Auto-generated method stub
		return false;
	}

}
