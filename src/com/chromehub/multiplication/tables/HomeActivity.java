package com.chromehub.multiplication.tables;

import android.os.Bundle;
import android.widget.AdapterView.OnItemClickListener;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class HomeActivity extends Activity {
	
	private String choice;
	private Intent intent;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ListView listView = (ListView) findViewById(R.id.listView1);
        String[] values = new String[] { "Refer", "Test", "Options"};
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1, android.R.id.text1, values);        
        listView.setAdapter(adapter);        
        listView.setOnItemClickListener(new OnItemClickListener() {
        	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        		choice = ((TextView)view).getText().toString();
        		if(choice.compareToIgnoreCase("Refer") == 0){
        			intent = new Intent(HomeActivity.this,ReferActivity.class); 
        	    	HomeActivity.this.startActivity(intent);
        		}
        		else if(choice.compareToIgnoreCase("Options") == 0){
        			intent = new Intent(HomeActivity.this,OptionsActivity.class); 
        	    	HomeActivity.this.startActivity(intent);
        		}
        		else if(choice.compareToIgnoreCase("Test") == 0){
        			intent = new Intent(HomeActivity.this,OptionsActivity.class); 
        	    	HomeActivity.this.startActivity(intent);
        		}
        	}
        }); 
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_home, menu);
        return true;
    }  
}
